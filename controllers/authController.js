const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET || 'secretword';

const User = require('../models/userModel');

const registration = async (req, res) => {
  const {email, password, role} = req.body;

  try {
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
      createdDate: new Date(Date.now()),
    });

    await user.save();
    res.status(200).json({message: 'User created successfully!'});
  } catch (err) {
    if (err.code === 11000) {
      return res.status(400).json({
        message: `User with email \'${req.body.email}\' already exists`,
      });
    }
  }
};

const login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      message: `Cannot find user with ${email}!`,
    });
  }

  const isValid = await bcrypt.compare(password, user.password);

  if (!isValid) {
    return res.status(400).json({message: `Invalid password!`});
  }

  const {_id, role, createdDate} = user;
  const jwtToken = jwt.sign({
    _id,
    email,
    role,
    createdDate,
  }, JWT_SECRET);

  res.status(200).json({
    message: 'Success',
    jwt_token: jwtToken,
  });
};

const forgotPassword = (req, res) => {
  res.status(200).json({
    message: 'New password was sent to your email address',
  });
};

module.exports = {registration, login, forgotPassword};
