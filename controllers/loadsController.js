const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');
const truckTypesDimensions =
  require('../routers/middlewares/truckTypesDimensions');

const addLoad = async (req, res) => {
  const {
    name, payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;
  const userId = req.userInfo._id;
  const userRole = req.userInfo.role;

  if (userRole === 'DRIVER') {
    return res.status(400).json({
      message: 'Cannot add load for user with role DRIVER',
    });
  }

  const load = new Load({
    created_by: userId,
    name: name,
    payload: payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions: dimensions,
    created_date: new Date(Date.now()),
  });

  await load.save();
  res.status(200).json({message: 'Load added successfully!'});
};

const getActiveLoad = async (req, res) => {
  const userId = req.userInfo._id;
  const userRole = req.userInfo.role;

  if (userRole === 'SHIPPER') {
    return res.status(400).json({
      message: 'Cannot get load for user with role SHIPPER',
    });
  }

  const activeLoad = await Load.findOne({assigned_to: userId}, {__v: 0});

  if (!activeLoad || activeLoad.status !== 'ASSIGNED') {
    return res.status(400).json({message: 'Cannot find active loads'});
  }

  res.status(200).json({load: activeLoad});
};

const iterateState = async (req, res) => {
  const userId = req.userInfo._id;
  const userRole = req.userInfo.role;

  if (userRole === 'SHIPPER') {
    return res.status(400).json({
      message: 'Cannot add load for user with role SHIPPER',
    });
  }

  const activeLoad = await Load.findOne({assigned_to: userId}, {__v: 0});

  if (!activeLoad) {
    return res.status(400).json({message: 'Cannot find active loads'});
  }

  const currentState = activeLoad.state;
  let newState = '';

  switch (currentState) {
    case 'En route to Pick Up':
      newState = 'Arrived to Pick Up';
      break;
    case 'Arrived to Pick Up':
      newState = 'En route to delivery';
      break;
    case 'En route to delivery':
      newState = 'Arrived to delivery';
      activeLoad.status = 'SHIPPED';
      const driver = await User.findOne({_id: activeLoad.assigned_to});
      const truck = await Truck.findOne({assigned_to: driver._id});
      truck.status = 'IS';
      await truck.save();
      break;
  }

  activeLoad.state = newState;
  activeLoad.logs.push({
    message: `Load state changed to '${newState}'`,
    time: Date.now(),
  });

  if (activeLoad.status === 'SHIPPED') {
    activeLoad.logs.push({
      message: 'Load shipped',
      time: Date.now(),
    });
  }

  await activeLoad.save();
  res.status(200).json({message: `Load state changed to '${newState}'`});
};

const getLoads = async (req, res) => {
  const userId = req.userInfo._id;
  const userRole = req.userInfo.role;
  const loadStatus = req.params.status ? req.params.status.toUpperCase() : null;
  const {limit = 10, offset = 0} = req.query;

  const conditions = {};
  if (loadStatus) {
    conditions['status'] = loadStatus;
  }

  if (userRole === 'SHIPPER') {
    conditions['created_by'] = userId;
    const shipperLoads = await Load.find(conditions, {__v: 0}, {
      limit: +limit > 50 ? 10 : +limit,
      skip: +offset,
      sort: {
        created_date: -1,
      },
    });

    res.status(200).json({
      loads: shipperLoads,
      limit,
      offset: +limit + +offset,
    });
  } else if (userRole === 'DRIVER') {
    conditions['assigned_to'] = userId;
    const driverLoads = await Load.find(conditions, {__v: 0}, {
      limit: +limit > 50 ? 10 : +limit,
      skip: +offset,
      sort: {
        created_date: -1,
      },
    });

    res.status(200).json({
      loads: driverLoads,
      limit,
      offset: +limit + +offset,
    });
  }
};

const getLoadById = async (req, res) => {
  const loadId = req.params.id;
  const load = await Load.findOne({_id: loadId}, {__v: 0});

  res.status(200).json({load});
};

const updateLoad = async (req, res) => {
  const userRole = req.userInfo.role;
  const loadId = req.params.id;
  const {
    name, payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;

  if (userRole === 'DRIVER') {
    return res.status(400).json({
      message: 'Cannot add load for user with role DRIVER',
    });
  }

  const load = await Load.findOne({_id: loadId});

  if (load.status !== 'NEW') {
    return res.status(400).json({message: 'Cannot update assigned load'});
  }

  load.name = name;
  load.payload = payload;
  load.pickup_address = pickupAddress;
  load.delivery_address = deliveryAddress;
  load.dimensions = dimensions;

  await load.save();
  res.status(200).json({message: 'Load updated successfully!'});
};

const deleteLoad = async (req, res) => {
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId});

  if (load.status !== 'NEW') {
    return res.status(400).json({message: 'Cannot delete assigned load'});
  }

  await Load.findByIdAndDelete({_id: loadId});
  res.status(200).json({message: 'Load deleted successfully!'});
};

const postLoad = async (req, res) => {
  const userRole = req.userInfo.role;
  const loadId = req.params.id;

  if (userRole === 'DRIVER') {
    return res.status(400).json({
      message: 'Cannot post load for user with role DRIVER',
    });
  }

  const load = await Load.findOne({_id: loadId});
  load.status = 'POSTED';
  load.logs.push({
    message: 'Searching for driver',
    time: Date.now(),
  });
  await load.save();

  const {
    width: loadWidth,
    length: loadLength,
    height: loadHeight,
  } = load.dimensions;
  const loadPayload = load.payload;

  let isDriverFound = false;
  await Truck.find({}, (err, trucks) => {
    if (err) {
      return res.status(400).json({message: err.message});
    }

    trucks.forEach((truck) => {
      const isTruckAssigned = truck.assigned_to;
      const truckStatus = truck.status;
      const truckType = truck.type;

      let doesLoadFit = false;
      const {
        width: truckWidth,
        length: truckLength,
        height: truckHeight,
        payload: truckPayload,
      } = truckTypesDimensions[truckType];

      if (truckWidth > loadWidth &&
          truckLength > loadLength &&
          truckHeight > loadHeight &&
          truckPayload > loadPayload) {
        doesLoadFit = true;
      }

      if (isTruckAssigned && truckStatus === 'IS' && doesLoadFit) {
        load.status = 'ASSIGNED';
        load.state = 'En route to Pick Up';
        load.assigned_to = truck['assigned_to'];
        load.logs.push({
          message: 'Driver found',
          time: Date.now(),
        }, {
          message: `Load state changed to 'En route to Pick Up'`,
          time: Date.now(),
        });

        truck.status = 'OL';
        truck.save();

        isDriverFound = true;
        return;
      }
    });
  });

  if (isDriverFound) {
    await load.save();
    return res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } else {
    load.status = 'NEW';
    load.logs.push({
      message: 'Driver not found',
      time: Date.now(),
    });

    await load.save();
    res.status(400).json({message: `Couldn't find suitable driver`});
  }
};

const getShippingInfo = async (req, res) => {
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId}, {__v: 0});

  if (load.status !== 'ASSIGNED') {
    return res.status(400).json({
      message: 'Cannot get info about inactive load',
    });
  }

  const driver = await User.findOne({_id: load.assigned_to});
  const truck = await Truck.findOne({assigned_to: driver._id}, {__v: 0});

  res.status(200).json({load, truck});
};

module.exports = {
  addLoad,
  getActiveLoad,
  iterateState,
  getLoads,
  getLoadById,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo,
};
