const User = require('../models/userModel');
const Truck = require('../models/truckModel');
const Load = require('../models/loadModel');
const bcrypt = require('bcrypt');

const getUser = async (req, res) => {
  const _id = req.userInfo._id;
  const user = await User.findById(_id);
  const {email, createdDate} = user;

  res.status(200).json({
    user: {
      _id,
      email,
      createdDate,
    },
  });
};

const deleteUser = async (req, res) => {
  const userId = req.userInfo._id;
  const role = req.userInfo.role;

  if (role === 'DRIVER') {
    return res.status(400).json({
      message: `Cannot delete account as user's role is DRIVER`,
    });
  }

  await User.findByIdAndDelete({_id: userId});
  await Load.deleteMany({created_by: userId});
  res.status(200).json({message: 'User has been deleted.'});
};

const updateUser = async (req, res) => {
  const userId = req.userInfo._id;
  const user = await User.findById({_id: userId});

  const assignedTruck = await Truck.findOne({assigned_to: userId});

  if (assignedTruck) {
    return res.status(400).json({
      message: 'Cannot update DRIVER with assigned truck',
    });
  }

  const {oldPassword, newPassword} = req.body;
  const isValidPassword = await bcrypt.compare(oldPassword, user.password);

  if (!isValidPassword) {
    return res.status(400).json({message: 'Invalid old password.'});
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
  res.status(200).json({message: 'Password has been changed.'});
};

module.exports = {
  getUser,
  deleteUser,
  updateUser,
};
