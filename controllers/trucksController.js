const Truck = require('../models/truckModel');

const addTruck = async (req, res) => {
  const type = req.body.type;
  const userId = req.userInfo._id;
  const userRole = req.userInfo.role;

  if (userRole === 'SHIPPER') {
    return res.status(400).json({
      message: 'Cannot add truck for user with role SHIPPER',
    });
  }

  const truck = new Truck({
    userId,
    created_by: userId,
    type: type,
    created_date: new Date(Date.now()),
  });

  await truck.save();
  res.status(200).json({message: 'Truck added successfully!'});
};

const getAllDriverTrucks = async (req, res) => {
  const createdBy = req.userInfo._id;
  const userRole = req.userInfo.role;
  const {limit = 6, offset = 0} = req.query;

  if (userRole === 'SHIPPER') {
    return res.status(400).json({
      message: 'Cannot get trucks for user with role SHIPPER',
    });
  }

  const driverTrucks = await Truck.find({created_by: createdBy}, {__v: 0}, {
    limit: +limit > 100 ? 6 : +limit,
    skip: +offset,
    sort: {
      created_date: -1,
    },
  });

  res.status(200).json({
    trucks: driverTrucks,
    limit,
    offset: +limit + +offset,
  });
};

const getTruckById = async (req, res) => {
  const truckId = req.params.id;
  const truck = await Truck.findOne({_id: truckId}, {__v: false});

  res.status(200).json({truck});
};

const updateTruck = async (req, res) => {
  const truckId = req.params.id;
  const type = req.body.type;
  const truck = await Truck.findOne({_id: truckId});

  if (truck.assigned_to) {
    return res.status(400).json({message: 'Cannot update assigned truck'});
  }

  truck.type = type;

  await truck.save();
  res.status(200).json({message: 'Truck updated successfully!'});
};

const deleteTruck = async (req, res) => {
  const truckId = req.params.id;

  const truck = await Truck.findOne({_id: truckId});

  if (truck.assigned_to) {
    return res.status(400).json({message: 'Cannot delete assigned truck'});
  }

  await Truck.findByIdAndDelete({_id: truckId});
  res.status(200).json({message: 'Truck deleted successfully!'});
};

const assignTruck = async (req, res) => {
  const truckId = req.params.id;
  const userId = req.userInfo._id;

  const assignedTruck = await Truck.findOne({
    assigned_to: userId, status: 'OL',
  });
  if (assignedTruck) {
    return res.status(400).json({message: 'Cannot assign more than one truck'});
  }

  const truck = await Truck.findOne({_id: truckId});
  truck.assigned_to = userId;

  await Truck.find({}, (err, trucks) => {
    if (err) {
      return res.status(400).json({message: err.message});
    }

    trucks.forEach((truck) => {
      if (truck.assigned_to && truck.status === 'IS') {
        truck.assigned_to = null;
        truck.save();
      }
    });
  });

  await truck.save();
  res.status(200).json({message: 'Truck assigned successfully!'});
};


module.exports = {
  addTruck,
  getAllDriverTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
};
