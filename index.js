require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

const port = process.env.PORT || 8080;
const app = express();

app.use(morgan('tiny'));
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://testuser:admin123@cluster0.mqjho.mongodb.net/truckservice?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  } catch (error) {
    console.log(error.message);
  }

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
