# NODEJS_HW3

UBER-like service for freight trucks. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money.

**Installation and set up:** run `npm install` and then `npm start`. Application will be running on port 8080.

**Prerequisites:** installed Node.js and MongoDB.