const Joi = require('joi');

const loadValidation = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number()
          .required(),
      length: Joi.number()
          .required(),
      height: Joi.number()
          .required(),
    }),
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};

module.exports = {loadValidation};
