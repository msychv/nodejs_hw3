const JWT = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET || 'secretword';

module.exports = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({
      message: 'No Authorization http header found',
    });
  }

  const [, jwtToken] = header.split(' ');

  await JWT.verify(jwtToken, JWT_SECRET, (err, data) => {
    if (err) {
      return res.status(400).json({message: 'No valid JWT token found!'});
    }

    req.userInfo = data;
    next();
  });
};
