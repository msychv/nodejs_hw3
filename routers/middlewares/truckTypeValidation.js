const Joi = require('joi');

module.exports = async function(req, res, next) {
  const schema = Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};
