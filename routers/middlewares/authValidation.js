const Joi = require('joi');

const registrationValidation = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),
    role: Joi.string()
        .valid('SHIPPER', 'DRIVER')
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};

const loginValidation = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};

const emailValidation = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
};

module.exports = {registrationValidation, loginValidation, emailValidation};
