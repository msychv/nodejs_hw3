const doesUserExist = (Model) => async (req, res, next) => {
  const userId = req.userInfo._id;

  if (!await Model.exists({_id: userId})) {
    return res.status(400).json({message: 'Cannot find such user'});
  }

  next();
};

const doesTruckExist = (Model) => async (req, res, next) => {
  const createdBy = req.userInfo._id;
  const truckId = req.params.id;

  if (!await Model.exists({created_by: createdBy, _id: truckId})) {
    return res.status(400).json({message: 'Cannot find such truck'});
  }

  next();
};

const doesLoadExist = (Model) => async (req, res, next) => {
  const createdBy = req.userInfo._id;
  const loadId = req.params.id;

  if (!await Model.exists({created_by: createdBy, _id: loadId})) {
    return res.status(400).json({message: 'Cannot find such load'});
  }

  next();
};

module.exports = {doesUserExist, doesTruckExist, doesLoadExist};
