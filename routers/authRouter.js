const express = require('express');
const router = new express.Router();
const {
  registration,
  login,
  forgotPassword,
} = require('../controllers/authController');
const {asyncWrapper} = require('./middlewares/helpers');
const {
  registrationValidation,
  loginValidation,
  emailValidation,
} = require('./middlewares/authValidation');

router.post('/register',
    asyncWrapper(registrationValidation),
    asyncWrapper(registration));

router.post('/login',
    asyncWrapper(loginValidation),
    asyncWrapper(login));

router.post('/forgot_password',
    asyncWrapper(emailValidation),
    asyncWrapper(forgotPassword));

module.exports = router;
