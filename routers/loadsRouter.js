const express = require('express');
const router = new express.Router();
const {
  addLoad, getActiveLoad, iterateState,
  getLoads, getLoadById, updateLoad,
  deleteLoad, postLoad, getShippingInfo,
} = require('../controllers/loadsController');
const loadModel = require('../models/loadModel');
const userModel = require('../models/userModel');
const {doesUserExist, doesLoadExist} = require('./middlewares/doesEntityExist');
const {asyncWrapper} = require('./middlewares/helpers');
const jwtVerification = require('./middlewares/jwtVerification');
const {loadValidation} = require('./middlewares/loadValidation');

router.post('/',
    asyncWrapper(loadValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(addLoad));

router.get('/active',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getActiveLoad));

router.patch('/active/state',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(iterateState));

router.get('/:status([a-z]+)?',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getLoads));

router.get('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesLoadExist(loadModel)),
    asyncWrapper(getLoadById));

router.put('/:id',
    asyncWrapper(loadValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesLoadExist(loadModel)),
    asyncWrapper(updateLoad));

router.delete('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesLoadExist(loadModel)),
    asyncWrapper(deleteLoad));

router.post('/:id/post',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesLoadExist(loadModel)),
    asyncWrapper(postLoad));

router.get('/:id/shipping_info',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesLoadExist(loadModel)),
    asyncWrapper(getShippingInfo));

module.exports = router;
