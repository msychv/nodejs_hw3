const express = require('express');
const router = new express.Router();
const {
  addTruck, getAllDriverTrucks, getTruckById,
  updateTruck, deleteTruck, assignTruck,
} = require('../controllers/trucksController');
const truckModel = require('../models/truckModel');
const userModel = require('../models/userModel');
const {
  doesUserExist,
  doesTruckExist,
} = require('./middlewares/doesEntityExist');
const {asyncWrapper} = require('./middlewares/helpers');
const jwtVerification = require('./middlewares/jwtVerification');
const truckTypeValidation = require('./middlewares/truckTypeValidation');

router.post('/',
    asyncWrapper(truckTypeValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(addTruck));

router.get('/',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getAllDriverTrucks));

router.get('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesTruckExist(truckModel)),
    asyncWrapper(getTruckById));

router.put('/:id',
    asyncWrapper(truckTypeValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesTruckExist(truckModel)),
    asyncWrapper(updateTruck));

router.delete('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesTruckExist(truckModel)),
    asyncWrapper(deleteTruck));

router.post('/:id/assign',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesTruckExist(truckModel)),
    asyncWrapper(assignTruck));

module.exports = router;
